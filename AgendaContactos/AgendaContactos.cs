﻿using System;
using System.Collections.Generic;

namespace AgendaContactos
{
    class AgendaContactos
    {
        public class Contacto
        {
            /*--- Atrivutos ---*/
            public string Name { get; set; }
            public string Numero { get; set; }

            /*---Constructores---*/
            public Contacto(string name, string numero)
            {
                Name = name;
                Numero = numero;
            }

            /*---Metodos---*/
            public bool Equals(Contacto con)
            {
                return this.Name == con.Name;
            }
            public override string ToString()
            {
                return $"{Name} tiene el numero {Numero}\n";
            }
        }
        public class Agenda
        {
            /*--- Atrivutos ---*/
            public List<Contacto> Contactos { get; set; }
            public int Longitud { get; set; }

            /*---Constructores---*/
            public Agenda()
            {
                Longitud = 10;
                Contactos = new List<Contacto> { };
            }
            public Agenda(int length)
            {
                Longitud = length;
                Contactos = new List<Contacto> { };
            }

            /*---Metodos---*/
            public string AniadirContacto(Contacto c)
            {
                if (Contactos.Count >= Longitud) return "No pots a faixir mes contactes";
                Contactos.Add(c);
                return "Se a afaixt el contacte";
            }
            public bool ExisteContacto(Contacto c)
            {
                return Contactos.Contains(c);
            }
            public override string ToString()
            {
                string s = "";
                Contactos.ForEach(x => s += x.ToString());
                return s;
            }
            public string BuscaContacto(string name)
            {
                Contacto contacto = Contactos.Find(x => x.Name.ToLower() == name.ToLower());
                return contacto.ToString();
            }
            public string EliminarContacto(Contacto c)
            {
                if (Contactos.Remove(c)) return "Conatcto eliminado";
                return "Contato no encontrado";
            }
            public bool AgendaLlena()
            {
                return Longitud == Contactos.Count;
            }
            public int HuecosLibres()
            {
                return Longitud - Contactos.Count;
            }

        }
        static void Main()
        {
            Contacto contacto = new Contacto("Didac", "608560608");
            Contacto contacto1 = new Contacto("Subi", "608325960");
            Agenda agenda = new Agenda(2);
            Console.WriteLine(agenda.AniadirContacto(contacto));
            Console.WriteLine(agenda.AniadirContacto(contacto1));
            Console.WriteLine(agenda.AniadirContacto(contacto));
            Console.WriteLine(agenda.AgendaLlena());
            Console.WriteLine(agenda.ToString());
            Console.WriteLine(agenda.HuecosLibres());
            Console.WriteLine(agenda.BuscaContacto("Didac"));
            Console.WriteLine(agenda.EliminarContacto(contacto1));
            Console.WriteLine(agenda.AgendaLlena());
            Console.WriteLine(agenda.ToString());
            Console.WriteLine(agenda.HuecosLibres());



        }
    }
}
