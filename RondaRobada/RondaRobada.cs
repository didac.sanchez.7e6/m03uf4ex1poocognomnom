﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace RondaRobada
{
    internal class RondaRobada
    {
        public class Cart
        {
            /*--- Atrivutos ---*/
            public int Numero { get; set; }
            public string Palo { get; set; }

            /*--- Constructor ---*/
            public Cart(int numero, string palo)
            {
                Numero = numero;
                Palo = palo;
            }

            /*--- Constructor ---*/
            public override string ToString()
            {
                return $"La carte tiene el numero {Numero} i el palo {Palo}";
            }
        }
        public class Barajas
        {
            /*--- Atrivutos ---*/
            public List<Cart> Baraja { get; set; }
            public List<Cart> Descart { get; set; }

            /*--- Constructor ---*/
            public Barajas()
            {
                Baraja= new List<Cart>();
                Descart= new List<Cart>();
                int[] numeros = { 1, 2, 3, 4, 5, 6, 7, 10, 11, 12 };
                string[] palos = { "espadas", "bastos", "oros", "copas" };
                for (int i = 0; i < numeros.Length; i++)
                {
                    for (int j = 0; j < palos.Length; j++)
                    {
                        Cart carte = new Cart(numeros[i], palos[j]);
                        Baraja.Add(carte);
                    }
                }
            }

            /*---Metodos---*/
            public void Barajar() // No funciona como deveria
            {
                var rnd = new Random();
                Baraja = Baraja.OrderBy(item => rnd.Next()).ToList();
            }
            public string SigienteCarta()
            {
                if (Baraja.Count > 0) return Baraja[Baraja.Count - 1].ToString();
                return "No hay mas cartas";
            }
            public int CartasDisponibles()
            {
                return Baraja.Count;
            }
            public List<Cart> DarCartas(int cantidad) 
            {
                List<Cart> carts = new List<Cart>();
                for (int i = 0; i < cantidad; i++)
                {
                    carts.Add(Baraja[Baraja.Count -1]);
                    Descart.Add(Baraja[Baraja.Count - 1]);
                    Baraja.Remove(Baraja[Baraja.Count - 1]);
                }
                return carts;
            }
            public string CartasMonton()
            {
                string carts = "";
                Descart.ForEach(item => { carts += item.ToString() + "\n"; });
                return carts;
            }
            public string MostrarBaraja()
            {
                string carts = "";
                Baraja.ForEach(item => { carts += item.ToString() +"\n"; });
                return carts;
            }
        }
        static void Main()
        {
            Barajas baraja= new Barajas();
            baraja.Barajar();
            Console.WriteLine(baraja.MostrarBaraja());
            baraja.DarCartas(5);
            Console.WriteLine(baraja.SigienteCarta());
            baraja.DarCartas(1);
            Console.WriteLine(baraja.CartasDisponibles());
            Console.WriteLine(baraja.CartasMonton());
        }
    }
}
