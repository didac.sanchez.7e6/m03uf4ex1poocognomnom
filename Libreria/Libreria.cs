﻿using System;

namespace Libreria
{
    class Libreria
    {
        public class Cuenta
        {
            /*---Atrivutos---*/
            public string Titular { get; set; }
            public double Cantidad { get; set; }
            /*---Constructores---*/
            public Cuenta(string Titular)
            {
                this.Titular = Titular;
            }
            public Cuenta(string Titular, double Cantidad)
            {
                this.Titular = Titular;
                this.Cantidad = Cantidad;
            }
            /*---Metodos---*/
            public override string ToString()
            {
                return $"Titular: {Titular}\nCantidad: {Cantidad}";
            }
            public double Ingresar(double ingres)
            {
                if (ingres >= 0) Cantidad += ingres;
                return Cantidad;
            }
            public double Retirar(double extracte)
            {
                Cantidad -= extracte;
                if (Cantidad <= 0) Cantidad = 0;
                return Cantidad;
            }
        }

        static void Main()
        {
            Console.WriteLine("AhorrosCuentaBancaria");
            Cuenta cuenta1 = new Cuenta("Angeles");
            Cuenta cuenta2 = new Cuenta("Fernando", 300);


            //Ingresa dinero en las cuentas
            cuenta1.Ingresar(300);
            cuenta2.Ingresar(400);


            //Retiramos dinero en las cuentas
            cuenta1.Retirar(500);
            cuenta2.Retirar(100);


            //Muestro la informacion de las cuentas
            Console.WriteLine(cuenta1.Cantidad); // 0 euros
            Console.WriteLine(cuenta2.Cantidad); // 600 euros


            //Muestro toda la informacion de las cuentas
            Console.WriteLine(cuenta1.ToString());
            Console.WriteLine(cuenta2.ToString());
            Console.WriteLine(cuenta1.ToString());
            Console.WriteLine(cuenta2.ToString());
        }
    }
}
