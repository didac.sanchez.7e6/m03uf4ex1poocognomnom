﻿using System;
using System.Collections.Generic;

namespace RuletaRusa
{
    internal class RuletaRusa
    {
        public class Revolver
        {
            /*--- Atrivutos ---*/
            public int PosicionActual { get; set; }
            public int PosicionBala { get; set; }

            /*---Constructores---*/
            public Revolver()
            {
                Random rng = new Random();
                PosicionActual = rng.Next(1, 7);
                PosicionBala = rng.Next(1, 7);
            }

            /*---Metodos---*/
            public bool Disparar()
            {
                return PosicionActual == PosicionBala;
            }
            public void SigienteBala()
            {
                PosicionActual += 1;
                if (PosicionActual >= 7) PosicionActual = 1;
            }
            public override string ToString()
            {
                return $"El tambor esta en la posicion {PosicionActual}, la bala esta en la posicion {PosicionBala}";
            }
        }
        public class Jugador
        {
            /*--- Atrivutos ---*/
            public int Id { get; set; }
            public string Nom { get; set; }
            public bool Vivo { get; set; }

            /*---Constructores---*/
            public Jugador()
            {
                Id = 1;
                Nom = "Jugador" + Id;
                Vivo = true;
            }
            public Jugador(int id)
            {
                Id = id;
                Nom = "Jugador" + Id;
                Vivo = true;
            }


            /*---Metodos---*/
            public string Disparar(Revolver r)
            {
                if (r.Disparar()) Vivo = false;
                r.SigienteBala();
                return this.ToString();
            }
            public override string ToString()
            {
                return $"El jugador {Nom} es {Vivo} que es viu \n";
            }
        }
        public class Juego
        {
            /*--- Atrivutos ---*/
            public List<Jugador> Jugadores { get; set; }
            public Revolver Revolver { get; set; }

            /*---Constructores---*/
            public Juego(int jugadores)
            {
                Jugadores = new List<Jugador> { };
                if (jugadores > 6 || jugadores < 0) jugadores = 6;
                for(int i = 1; i <= jugadores; i++){
                    Jugadores.Add(new Jugador(i));
                }
                Revolver = new Revolver();
            }



            /*---Metodos---*/
            public string Ronda()
            {
                string s = "";
                Jugadores.ForEach(x => x.Disparar(Revolver));
                Jugadores.ForEach(x => s+= x.ToString());
                return s;
            }
            public bool FinJuego()
            {
                return Jugadores.TrueForAll(b => b.Vivo == true);
            }
            
        }

        static void Main()
        {
            Juego juego1 = new Juego(9);
            Juego juego2 = new Juego(2);
            Console.WriteLine(juego1.Ronda());
            Console.WriteLine(juego1.FinJuego());
            Console.WriteLine(juego2.Ronda());
            Console.WriteLine(juego2.FinJuego());
        }
    }
}
