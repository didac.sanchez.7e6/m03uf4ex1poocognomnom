﻿using System;

namespace ExPOO
{
    class CuentaBancaria
    {
        public class Cuenta
        {
            public string Titular { get; set; }
            public double Cantidad { get; set; }
            public Cuenta(string titular)
            {
                this.Titular = titular;
            }
            public Cuenta(string titular, double cantidad)
            {
                this.Titular = titular;
                this.Cantidad = cantidad;
            }
            public double Ingresar(double ingres)
            {
                if (ingres >= 0) Cantidad += ingres;
                return Cantidad;
            }
            public double Retirar(double extracte)
            {
                Cantidad -= extracte;
                if (Cantidad <= 0) Cantidad = 0;
                return Cantidad;
            }
        }

        static void Main()
        {
            Console.WriteLine("AhorrosCuentaBancaria");
            Cuenta cuenta1 = new Cuenta("Angeles");
            Cuenta cuenta2 = new Cuenta("Fernando", 300);


            //Ingresa dinero en las cuentas
            cuenta1.Ingresar(300);
            cuenta2.Ingresar(400);


            //Retiramos dinero en las cuentas
            cuenta1.Retirar(500);
            cuenta2.Retirar(100);


            //Muestro la informacion de las cuentas
            Console.WriteLine(cuenta1.Cantidad); // 0 euros
            Console.WriteLine(cuenta2.Cantidad); // 600 euros
        }
    }
}
