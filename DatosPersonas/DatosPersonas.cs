﻿using System;

namespace DatosPersonas
{
    class DatosPersonas
    {
        public class Persona
        {
            /*--- Atrivutos ---*/
            public string Nom { get; set; }
            public int Edad { get; set; }
            public string DNI { get; set; }
            public char Sexo { get; set; }
            public double Altura { get; set; }
            public double Peso { get; set; }

            /*--- Constructores ---*/

            public Persona()
            {
                this.Nom = "";
                this.Edad = 0;
                this.DNI = GeneraDNI();
                this.Sexo = 'H';
                this.Peso = 0;
                this.Altura = 0;
            }
            public Persona(string nom, int edad, char sexo)
            {
                Nom = nom;
                Edad = edad;
                DNI = GeneraDNI();
                this.Sexo = ComprobarSexo(sexo);
                Altura = 0;
                Peso = 0;
            }
            public Persona(string nom, int edad, char sexo, double altura, double peso)
            {
                Nom = nom;
                Edad = edad;
                DNI = GeneraDNI();
                this.Sexo = ComprobarSexo(sexo);
                Altura = altura;
                Peso = peso;
            }

            /*--- Set ---*/
            public void SetSexo(char sexo)
            {
                this.Sexo = ComprobarSexo(sexo);
            }

            /*--- Metodos ---*/
            public int CalcularIMC()
            {
                const int pesoMinimo = 20;
                const int pesoMaximo = 25;
                double IMC = Peso / Math.Pow(Altura, 2);
                if (IMC < pesoMinimo) return -1;
                if (IMC >= pesoMinimo && IMC <= pesoMaximo) return 0;
                else return 1;
            }
            public bool EsMayorDeEdad()
            {
                return Edad >= 18;
            }
            public char ComprobarSexo(char sexo)
            {
                if (sexo != 'H' || sexo != 'M') return 'H';
                return sexo;
            }
            public override string ToString()
            {
                return $"La persona {Nom}, con el DNI {DNI}, tiene {Edad} años, es {Sexo}, pesa {Peso} Kg y mide {Altura}";
            }
            public string GeneraDNI()
            {
                Random rng = new Random();
                int numeroDNI = rng.Next(10000000, 100000000);
                const string letraDNI = "TRWAGMYFPDXBNJZSQVHLCKE";
                return Convert.ToString(numeroDNI) + letraDNI[numeroDNI % 23];
            }

        }
        class Ejecutable
        {
            static void Main()
            {
                string nombre = AskString("El nombre");
                string edad;
                int edadNumero;
                do
                {
                    edad = AskString("La edad");
                } while (!int.TryParse(edad, out edadNumero));
                string sexo;
                char sexoChar;
                do
                {
                    sexo = AskString("El sexo");
                } while (!char.TryParse(sexo.ToUpper(), out sexoChar));
                string peso;
                double pesoDouble;
                do
                {
                    peso = AskString("El peso");
                } while (!double.TryParse(peso, out pesoDouble));
                string altura;
                double alturaDouble;
                do
                {
                    altura = AskString("La altura");
                } while (!double.TryParse(altura, out alturaDouble));
                Persona personaVacia = new Persona();
                Persona personaTotal = new Persona(nombre,edadNumero,sexoChar,alturaDouble,pesoDouble);
                Persona personaNES = new Persona(nombre, edadNumero, sexoChar);

                personaVacia.Nom = "Livic";
                personaVacia.Edad = 18;
                personaVacia.Sexo = 'D';
                personaVacia.Altura = 1.80;
                personaVacia.Peso = 80;

                Console.WriteLine(personaVacia.CalcularIMC());
                Console.WriteLine(personaTotal.CalcularIMC());
                Console.WriteLine(personaNES.CalcularIMC());

                Console.WriteLine(personaVacia.EsMayorDeEdad());
                Console.WriteLine(personaTotal.EsMayorDeEdad());
                Console.WriteLine(personaNES.EsMayorDeEdad());

                Console.WriteLine(personaVacia.ToString());
                Console.WriteLine(personaTotal.ToString());
                Console.WriteLine(personaNES.ToString());
            }
            static string AskString(string s)
            {
                Console.WriteLine(s);
                return Console.ReadLine();
            }

        }
    }
}
